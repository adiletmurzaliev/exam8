<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rent
 *
 * @ORM\Table(name="rent")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RentRepository")
 */
class Rent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="datetime")
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="datetime")
     */
    private $endDate;

    /**
     * @var Book
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Book", inversedBy="rents")
     */
    private $book;

    /**
     * @var Ticket
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Ticket", inversedBy="rents")
     */
    private $ticket;


    public function __construct()
    {
        $this->startDate = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Rent
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Rent
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param Book $book
     * @return Rent
     */
    public function setBook(Book $book): ?Rent
    {
        $this->book = $book;
        return $this;
    }

    /**
     * @return Book
     */
    public function getBook(): ?Book
    {
        return $this->book;
    }

    /**
     * @param Ticket $ticket
     * @return Rent
     */
    public function setTicket(Ticket $ticket): ?Rent
    {
        $this->ticket = $ticket;
        return $this;
    }

    /**
     * @return Ticket
     */
    public function getTicket(): ?Ticket
    {
        return $this->ticket;
    }
}

