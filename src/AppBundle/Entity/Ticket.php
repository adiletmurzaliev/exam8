<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Ticket
 *
 * @ORM\Table(name="ticket")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TicketRepository")
 * @UniqueEntity("passport", message="validation.passport_not_unique")
 */
class Ticket
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fullName", type="string", length=255)
     * @Assert\NotBlank(message="validation.not_blank")
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     * @Assert\NotBlank(message="validation.not_blank")
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="passport", type="string", length=255)
     * @Assert\NotBlank(message="validation.not_blank")
     */
    private $passport;

    /**
     * @var string
     *
     * @ORM\Column(name="ticketNumber", type="string", length=255)
     */
    private $ticketNumber;

    /**
     * @var bool
     *
     * @ORM\Column(name="isViewed", type="boolean")
     */
    private $isViewed;

    /**
     * @var Rent[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Rent", mappedBy="ticket")
     */
    private $rents;


    public function __construct()
    {
        $this->ticketNumber = uniqid($this->getId());
        $this->isViewed = false;
        $this->rents = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fullName
     *
     * @param $fullName
     * @return Ticket
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Ticket
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set passport
     *
     * @param string $passport
     *
     * @return Ticket
     */
    public function setPassport($passport)
    {
        $this->passport = $passport;

        return $this;
    }

    /**
     * Get passport
     *
     * @return string
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * @return string
     */
    public function getTicketNumber(): string
    {
        return $this->ticketNumber;
    }

    /**
     * @param bool $isViewed
     * @return Ticket
     */
    public function setIsViewed(bool $isViewed): Ticket
    {
        $this->isViewed = $isViewed;
        return $this;
    }

    /**
     * @return bool
     */
    public function isViewed(): bool
    {
        return $this->isViewed;
    }

    /**
     * @return Rent[]|ArrayCollection
     */
    public function getRents()
    {
        return $this->rents;
    }

    /**
     * @param Rent $rent
     */
    public function addRent(Rent $rent)
    {
        $this->rents->add($rent);
    }

    /**
     * @param Rent $rent
     */
    public function removeRent(Rent $rent)
    {
        $this->rents->removeElement($rent);
    }

    public function __toString()
    {
        return $this->fullName;
    }
}

