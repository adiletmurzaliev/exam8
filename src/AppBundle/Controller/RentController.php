<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Book;
use AppBundle\Entity\Rent;
use AppBundle\Entity\Ticket;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RentController extends Controller
{
    /**
     * @Route("/order/{id}", requirements={"id" : "\d+"}, name="rent_order")
     * @Method("GET")
     *
     * @param Book $book
     * @return Response
     */
    public function orderAction(Book $book): Response
    {
        if (!$book->isAvailable()) {
            return $this->redirectToRoute('main_index');
        }

        $form = $this->createFormBuilder()
            ->add('ticketNumber', TextType::class, ['label' => false])
            ->add('endDate', DateType::class, ['label' => 'rent.form.end_date'])
            ->add('bookId', HiddenType::class, ['data' => $book->getId()])
            ->add('submit', SubmitType::class, ['label' => 'rent.form.submit'])
            ->setAction($this->generateUrl('rent_create_order'))
            ->getForm();

        return $this->render('@App/Rent/order.html.twig', [
            'form' => $form->createView(),
            'book' => $book
        ]);
    }

    /**
     * @Route("/create-order", name="rent_create_order")
     * @Method("POST")
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function createOrderAction(Request $request, EntityManagerInterface $em): Response
    {
        $data = $request->request->get('form');
        $book = $this->getDoctrine()->getRepository(Book::class)->find($data['bookId']);
        $ticket = $this->getDoctrine()->getRepository(Ticket::class)->getTicketByNumber($data['ticketNumber']);

        if ($ticket && $book) {
            $rent = new Rent();
            $month = $data['endDate']['month'];
            $year = $data['endDate']['year'];
            $day = $data['endDate']['day'];
            $endDate = new \DateTime("{$year}-{$month}-{$day}");
            $rent
                ->setBook($book)
                ->setTicket($ticket)
                ->setEndDate($endDate);
            $book->setIsAvailable(false);
            $book->setEndDate($endDate);

            $em->persist($rent);
            $em->persist($book);
            $em->flush();

            return $this->redirectToRoute('main_index');
        }

        return $this->redirectToRoute('rent_order', ['id' => $book->getId()]);
    }

    /**
     * @Route("/show", name="rent_show")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function showAction(Request $request): Response
    {
        $form = $this->createFormBuilder()
            ->add('ticketNumber', TextType::class, ['label' => false])
            ->add('submit', SubmitType::class, ['label' => 'rent.form.submit'])
            ->getForm();

        $form->handleRequest($request);
        $result = [];

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $ticket = $this->getDoctrine()->getRepository(Ticket::class)->getTicketByNumber($data['ticketNumber']);
            if ($ticket) {

                $books = $this->getDoctrine()->getRepository(Book::class)->getAllByTicket($ticket);

                /** @var Book $book */
                foreach ($books as $book){
                    /** @var Rent $latestRent */
                    $latestRent = $this->getDoctrine()->getRepository(Rent::class)->getLastEndDate($book, $ticket);

                    if ($book->isAvailable()){
                        $status = $this->get('translator')->trans('rent.show_form.status_returned');
                    } elseif (new \DateTime() > $latestRent->getEndDate() ){
                        $status = $this->get('translator')->trans('rent.show_form.status_expired');
                    } else{
                        $status = $this->get('translator')->trans('rent.show_form.status_waiting');
                    }

                    $result[] = [
                        'book' => $book,
                        'status' => $status
                    ];
                }
            }
        }

        return $this->render('@App/Rent/show.html.twig', [
            'form' => $form->createView(),
            'result' => $result,
        ]);
    }
}
