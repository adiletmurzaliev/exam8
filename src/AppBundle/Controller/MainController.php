<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Book;
use AppBundle\Entity\Category;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class MainController extends Controller
{
    /**
     * @Route("/", name="main_index")
     * @Method("GET")
     *
     * @return Response
     */
    public function indexAction()
    {
        $activeCategory = $this->getDoctrine()
            ->getRepository(Category::class)
            ->getCategoryAll();

        $categories = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findAll();

        $books = $this->getDoctrine()
            ->getRepository(Book::class)
            ->getAllByCategory($activeCategory);

        return $this->render('@App/Main/index.html.twig', [
            'categories' => $categories,
            'active_category' => $activeCategory,
            'books' => $books
        ]);
    }

    /**
     * @Route("/cat/{id}", requirements={"id" : "\d+"}, name="main_show")
     * @Method("GET")
     *
     * @param Category $activeCategory
     * @return Response
     */
    public function showAction(Category $activeCategory): Response
    {
        $categories = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findAll();

        $books = $this->getDoctrine()
            ->getRepository(Book::class)
            ->getAllByCategory($activeCategory);

        return $this->render('@App/Main/index.html.twig', [
            'categories' => $categories,
            'active_category' => $activeCategory,
            'books' => $books
        ]);
    }

    /**
     * @Route("/locale/{_locale}/{route}", name="main_change_locale")
     * @Method("GET")
     *
     * @param string $route
     * @return Response
     */
    public function changeLocaleAction(string $route): Response
    {
        return $this->redirectToRoute('main_index');
    }

}
