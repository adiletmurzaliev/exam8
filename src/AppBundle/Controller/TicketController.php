<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Ticket;
use AppBundle\Form\TicketType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TicketController extends Controller
{
    /**
     * @Route("/register", name="ticket_register")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function registerAction(Request $request, EntityManagerInterface $em): Response
    {
        $ticket = new Ticket();

        $form = $this->createForm(TicketType::class, $ticket);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($ticket);
            $em->flush();

            return $this->redirectToRoute('ticket_registration_success', ['id' => $ticket->getId()]);
        }

        return $this->render('@App/Ticket/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/registration-success/{id}", requirements={"id" : "\d+"}, name="ticket_registration_success")
     * @Method("GET")
     *
     * @param Ticket $ticket
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function registrationSuccessAction(Ticket $ticket, EntityManagerInterface $em): Response
    {
        if ($ticket->isViewed()) {
            return $this->redirectToRoute('main_index');
        }

        $ticket->setIsViewed(true);
        $em->persist($ticket);
        $em->flush();

        return $this->render('@App/Ticket/registration_success.html.twig', [
            'ticket' => $ticket
        ]);
    }

}
