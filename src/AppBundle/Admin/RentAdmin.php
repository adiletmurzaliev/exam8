<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class RentAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('startDate')
            ->add('endDate')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('startDate')
            ->add('endDate')
            ->add('book', EntityType::class, [
                'class' => 'AppBundle\Entity\Book'
            ])
            ->add('ticket', EntityType::class, [
                'class' => 'AppBundle\Entity\Ticket'
            ])

            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id')
            ->add('startDate')
            ->add('endDate')
            ->add('book', EntityType::class, [
                'class' => 'AppBundle\Entity\Book'
            ])
            ->add('ticket', EntityType::class, [
                'class' => 'AppBundle\Entity\Ticket'
            ])
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('startDate')
            ->add('endDate')
            ->add('book', EntityType::class, [
                'class' => 'AppBundle\Entity\Book'
            ])
            ->add('ticket', EntityType::class, [
                'class' => 'AppBundle\Entity\Ticket'
            ])
        ;
    }
}
