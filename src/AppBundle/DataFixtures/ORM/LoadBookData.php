<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Book;
use AppBundle\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class LoadBookData extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $this->clearImages();
        $data = $this->generateData();

        foreach ($data as $item) {
            $book = new Book();
            $book
                ->setAuthor($item['author'])
                ->setTitle($item['title'])
                ->setImageFile(new UploadedFile($item['image'], $item['id'], null, null, null, true));

            foreach ($item['categories'] as $cat){
                /** @var Category $cat */
                $cat = $this->getReference($cat);
                $book->addCategory($cat);
            }

            $manager->persist($book);
            $manager->flush();
        }
    }

    private function generateData(): array
    {
        $fixturesPath = __DIR__ . '/../../../../web/fixtures/';

        // Copying fixtures images
        copy($fixturesPath . 'fx1.jpg', $fixturesPath . 'book1.jpg');
        copy($fixturesPath . 'fx1.jpg', $fixturesPath . 'book1_1.jpg');
        copy($fixturesPath . 'fx2.jpg', $fixturesPath . 'book2.jpg');
        copy($fixturesPath . 'fx3.jpg', $fixturesPath . 'book3.jpg');
        copy($fixturesPath . 'fx4.jpg', $fixturesPath . 'book4.jpg');

        return [
            [
                'id' => 'book1',
                'image' => $fixturesPath . 'book1.jpg',
                'title' => 'Книга первая',
                'author' => 'Дж. К. Роулинг',
                'categories' => [LoadCategoryData::CAT_ALL, LoadCategoryData::CAT_FANTASY]
            ],
            [
                'id' => 'book1_1',
                'image' => $fixturesPath . 'book1_1.jpg',
                'title' => 'Книга первая',
                'author' => 'Дж. К. Роулинг',
                'categories' => [LoadCategoryData::CAT_ALL, LoadCategoryData::CAT_FANTASY]
            ],
            [
                'id' => 'book2',
                'image' => $fixturesPath . 'book2.jpg',
                'title' => 'История России: С древнейших времен до наших дней',
                'author' => 'А. Н. Сахарова',
                'categories' => [LoadCategoryData::CAT_ALL, LoadCategoryData::CAT_FANTASY]
            ],
            [
                'id' => 'book3',
                'image' => $fixturesPath . 'book3.jpg',
                'title' => 'Книга вторая',
                'author' => 'Дж. К. Роулинг',
                'categories' => [LoadCategoryData::CAT_ALL, LoadCategoryData::CAT_HISTORY]
            ],
            [
                'id' => 'book4',
                'image' => $fixturesPath . 'book4.jpg',
                'title' => 'Книга третья',
                'author' => 'Дж. К. Роулинг',
                'categories' => [LoadCategoryData::CAT_ALL, LoadCategoryData::CAT_TEEN]
            ]
        ];
    }

    private function clearImages()
    {
        $files = glob(__DIR__ . '/../../../../web/uploads/images/books/*');
        foreach ($files as $file) {
            if (is_file($file))
                unlink($file);
        }
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    function getDependencies()
    {
        return [
            LoadCategoryData::class
        ];
    }
}