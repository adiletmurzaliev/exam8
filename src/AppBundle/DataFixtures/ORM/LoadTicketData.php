<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Ticket;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadTicketData extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $ticket = new Ticket();
        $ticket
            ->setFullName('User')
            ->setAddress('600 Markley st.')
            ->setPassport('AN7623')
        ;

        $manager->persist($ticket);
        $manager->flush();
    }

}