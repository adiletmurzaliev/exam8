<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCategoryData extends Fixture
{

    const CAT_ALL = 'All';
    const CAT_TEEN = 'Teen';
    const CAT_HISTORY = 'History';
    const CAT_FANTASY = 'Fantasy';

    public function load(ObjectManager $manager)
    {
        $cat1 = new Category();
        $cat1
            ->setTitle('Все');

        $this->addReference(self::CAT_ALL, $cat1);
        $manager->persist($cat1);

        $cat2 = new Category();
        $cat2
            ->setTitle('Для подростков');

        $this->addReference(self::CAT_TEEN, $cat2);
        $manager->persist($cat2);

        $cat3 = new Category();
        $cat3
            ->setTitle('Фэнтэзи');

        $this->addReference(self::CAT_FANTASY, $cat3);
        $manager->persist($cat3);

        $cat4 = new Category();
        $cat4
            ->setTitle('Документальные');

        $this->addReference(self::CAT_HISTORY, $cat4);
        $manager->persist($cat4);

        $manager->flush();
    }
}